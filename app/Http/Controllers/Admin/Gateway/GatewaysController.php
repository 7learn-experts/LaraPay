<?php

namespace App\Http\Controllers\Admin\Gateway;

use App\Filters\GatewayFilters;
use App\Models\Gateway;
use App\Repositories\Contracts\GatewayRepositoryInterface;
use App\Repositories\Contracts\PlanRepositoryInterface;
use App\Services\Gateway\CreateGatewayRequest;
use App\Services\Gateway\CreateGatewayService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GatewaysController extends Controller
{
    /**
     * @var GatewayRepositoryInterface
     */
    private $gatewayRepository;
    /**
     * @var PlanRepositoryInterface
     */
    private $plan_repository;

    public function __construct(
        GatewayRepositoryInterface $gatewayRepository,
        PlanRepositoryInterface $plan_repository
    ) {

        $this->gatewayRepository = $gatewayRepository;
        $this->plan_repository = $plan_repository;
    }

    public function index()
    {

        $gateways = Gateway::filters(new GatewayFilters())->get(); //$this->gatewayRepository->all(null, ['user', 'plan']);

        return view('admin.gateway.index', compact('gateways'));
    }

    public function create()
    {
        $statuses = $this->gatewayRepository->getStatuses();
        $plans = $this->plan_repository->all();
        $gatewayItem = null;

        return view('admin.gateway.create', compact('statuses', 'gatewayItem', 'plans'));

    }

    public function store(Request $request)
    {
        $createGatewayService = new CreateGatewayService(
            new CreateGatewayRequest(
                [
                    'user' => $request->input('owner'),
                    'plan' => $request->input('plan'),
                    'title' => $request->input('title'),
                    'website' => $request->input('website'),
                    'status' => $request->input('status')
                ]
            )
        );
        $newGateway = $createGatewayService->perform();
        if ($newGateway) {
            return back()->with('success', 'درگاه جدید با موفقیت ثبت گردید');
        }
    }

    public function search(Request $request)
    {
        $term = $request->search;
        $results = $this->gatewayRepository->search($term);
        return response()->json(['items' => $results]);
    }
}
