<?php

namespace App\Http\Controllers\Admin\Payment;

use App\Repositories\Contracts\PaymentRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentsController extends Controller
{
    /**
     * @var PaymentRepositoryInterface
     */
    private $payment_repository;

    public function __construct(PaymentRepositoryInterface $payment_repository)
    {

        $this->payment_repository = $payment_repository;
    }

    public function index()
    {
        $payments = $this->payment_repository->all();

        return view('admin.payment.index', compact('payments'));
    }
}
