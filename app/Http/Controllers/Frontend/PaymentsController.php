<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\Contracts\BankTransactionRepositoryInterface;
use App\Repositories\Contracts\PaymentRepositoryInterface;
use App\Repositories\Eloquent\Payment\PaymentType;
use App\Repositories\Eloquent\Transaction\BankTransactionStatus;
use App\Repositories\Eloquent\Transaction\EloquentBankTransactionRepository;
use App\Repositories\Eloquent\Transaction\GatewayTransactionStatus;
use App\Services\Payment\PaymentService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentsController extends Controller
{

    /**
     * @var PaymentService
     */
    private $paymentService;
    /**
     * @var PaymentRepositoryInterface
     */
    private $paymentRepository;
    /**
     * @var BankTransactionRepositoryInterface
     */
    private $bankTransactionRepository;

    public function __construct(
        PaymentService $paymentService,
        PaymentRepositoryInterface $paymentRepository,
        BankTransactionRepositoryInterface $bankTransactionRepository
    ) {

        $this->paymentService = $paymentService;
        $this->paymentRepository = $paymentRepository;
        $this->bankTransactionRepository = $bankTransactionRepository;
    }

    public function start(Request $request)
    {
//        $paymentKey = $request->payment_key;
        return $this->paymentService->doPayment(1, PaymentType::CASH);
    }

    public function verify(Request $request, $payment_code)
    {

        $params = $request->all();
        $params['paymentCode'] = $payment_code;
        $payment = $this->paymentRepository->findBy([
            'payment_code' => $payment_code
        ]);
        if (!$payment) {
            abort(404);
        }
        $transaction = $payment->transaction;
        $transaction->updateCallBackData($params);
        $paymentCheckStatus = $this->paymentService->checkPayment($params);
        $gatewayUpdate = [];
        $bankTransactionUpdate = [];
        if ($paymentCheckStatus) {
            $gatewayUpdate = [
                'gateway_transaction_status' => GatewayTransactionStatus::VERIFY_WAITING
            ];
            $bankTransactionUpdate = [
                'bank_transaction_status'  => BankTransactionStatus::VERIFY_WAITING
            ];

        } else {
            $gatewayUpdate = [
                'gateway_transaction_status' => GatewayTransactionStatus::FAILED
            ];
            $bankTransactionUpdate = [
                'bank_transaction_status'  => BankTransactionStatus::FAILED
            ];
        }
        $payment->transaction->gateway_transaction->update($gatewayUpdate);
        $this->bankTransactionRepository->update($payment->payment_bank_transaction_id,$bankTransactionUpdate);
        $callbackData = [
            'callbackUrl' => $transaction->gateway_transaction->gateway_transaction_callback_url,
            'transactionKey' => $transaction->gateway_transaction->gateway_transaction_key,
            'amount' => $transaction->gateway_transaction->gateway_transaction_amount,
            'resNumber' => $transaction->gateway_transaction->gateway_transaction_res_number,
            'status' => $paymentCheckStatus ? "OK" : "FAILED"
        ];
        return view('transaction.payment-callback', compact('callbackData'));
//        $verifyResult = $this->paymentService->verifyPayment($params);
//        return view('payment.verify', compact('verifyResult'));
    }
}
