<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserAccountResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'account_id' => $this->user_account_id,
            'account_title' => $this->user_account_title,
            'account_card_number' => $this->user_account_card_number
        ];
    }
}
