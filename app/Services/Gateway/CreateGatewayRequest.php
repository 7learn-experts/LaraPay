<?php


namespace App\Services\Gateway;


class CreateGatewayRequest
{
    private $userID;
    private $planID;
    private $title;
    private $website;
    private $status;
    private $bank;

    public function __construct(array $data)
    {
        $this->userID = $data['user'];
        $this->planID = $data['plan'];
        $this->title = $data['title'];
        $this->website = $data['website'];
        $this->status = $data['status'];
        $this->bank = 1;
    }

    /**
     * @return mixed
     */
    public function getUserID()
    {
        return $this->userID;
    }

    /**
     * @return mixed
     */
    public function getPlanID()
    {
        return $this->planID;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return mixed
     */
    public function getBank()
    {
        return $this->bank;
    }
}