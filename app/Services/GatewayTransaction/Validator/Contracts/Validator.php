<?php


namespace App\Services\GatewayTransaction\Validator\Contracts;


use App\Services\GatewayTransaction\TransactionRequest;

abstract class Validator
{
    /**
     * @var Validator
     */
    protected $nextValidator;

    public function __construct(Validator $validator = null)
    {
        $this->nextValidator = $validator;
    }

    final public function handle(TransactionRequest $request)
    {
        $result = $this->process($request);
        if ($result) {
            if (!is_null($this->nextValidator)) {
                return $this->nextValidator->handle($request);
            }
            return $result;
        }
        return $result;

    }

    abstract protected function process(TransactionRequest $request);
}