<?php


namespace App\Services\GatewayTransaction\Validator\Handlers;


use App\Repositories\Contracts\GatewayRepositoryInterface;
use App\Services\GatewayTransaction\TransactionRequest;
use App\Services\GatewayTransaction\Validator\Contracts\Validator;
use App\Services\GatewayTransaction\Validator\Exceptions\InvalidIPException;

class IPValidator extends Validator
{

    protected function process(TransactionRequest $request)
    {
        $gateway_repository = resolve(GatewayRepositoryInterface::class);
        $gateway = $gateway_repository->findBy([
            'gateway_access_token' => $request->getToken()
        ]);
        if($gateway->gateway_server_ip !== $request->getIp())
        {
            throw new InvalidIPException('invalid server ip address');
        }
        return true;
    }
}