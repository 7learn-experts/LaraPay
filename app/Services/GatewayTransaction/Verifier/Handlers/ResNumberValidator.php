<?php


namespace App\Services\GatewayTransaction\Verifier\Handlers;


use App\Repositories\Contracts\GatewayTransactionRepositoryInterface;
use App\Services\GatewayTransaction\TransactionVerifyRequest;
use App\Services\GatewayTransaction\Verifier\Contracts\Verifier;
use App\Services\GatewayTransaction\Verifier\Exceptions\InvalidResNumberException;

class ResNumberValidator extends Verifier
{

    protected function process(TransactionVerifyRequest $request)
    {
        $gatewayTransactionRepository = resolve(GatewayTransactionRepositoryInterface::class);
        $transaction =$gatewayTransactionRepository->findBy([
            'gateway_transaction_key' => $request->getTransactionKey(),
            'gateway_transaction_res_number' => $request->getResNumber()
        ]);
        if(is_null($transaction))
        {
            throw new InvalidResNumberException('invalid transaction or res number!');
        }
        return true;
    }
}