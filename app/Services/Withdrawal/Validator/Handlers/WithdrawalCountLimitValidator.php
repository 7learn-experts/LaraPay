<?php


namespace App\Services\Withdrawal\Validator\Handlers;


use App\Repositories\Contracts\WithdrawalRepositoryInterface;
use App\Services\Withdrawal\Validator\Contracts\Validator;
use App\Services\Withdrawal\Validator\Exceptions\WithdrawalLimitCountException;
use App\Services\Withdrawal\WithdrawalRequest;

class WithdrawalCountLimitValidator extends Validator
{

    protected function process(WithdrawalRequest $request)
    {
        $withdrawalRepository = resolve(WithdrawalRepositoryInterface::class);
        $withdrawalCount = $withdrawalRepository->getUserAccountWithdrawalCount($request->getAccount());
        $limitCount = $request->getRate();//config('constants.withdrawal.count_limit');
        if($withdrawalCount >= $limitCount)
        {
            throw new WithdrawalLimitCountException('تعداد دفعات مجاز ثبت درخواست واریز برای شما پر شده است');
        }
        return true;

    }
}