<?php


namespace App\Services\Withdrawal\Validator;


use App\Services\Withdrawal\Validator\Handlers\WithdrawalAmountValidator;
use App\Services\Withdrawal\Validator\Handlers\WithdrawalCountLimitValidator;
use App\Services\Withdrawal\Validator\Handlers\WithdrawalGatewayBalanceValidator;
use App\Services\Withdrawal\Validator\Handlers\WithdrawalMaxAmountValidator;
use App\Services\Withdrawal\WithdrawalRequest;

class WithdrawalValidator
{
    public function __construct()
    {

    }

    public function validate(WithdrawalRequest $request)
    {
        $limitCountValidator = new WithdrawalCountLimitValidator();
        $amountValidator = new WithdrawalAmountValidator($limitCountValidator);
        $maxAmountValidator = new WithdrawalMaxAmountValidator($amountValidator);
        $gatewayBalanceValidator = new WithdrawalGatewayBalanceValidator($maxAmountValidator);
        return $gatewayBalanceValidator->handle($request);

    }
}