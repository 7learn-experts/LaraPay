<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->string('payment_code', 30)->nullable()->after('payment_id');
            $table->string('payment_gateway', 100)->nullable()->after('payment_code');
            $table->string('payment_gateway_callback_state', 50)->nullable()->after('payment_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropColumn('payment_code');
            $table->dropColumn('payment_gateway');
            $table->dropColumn('payment_gateway_callback_state');
        });
    }
}
