<?php


namespace Tests\Integration\Models;


use App\Models\Gateway;
use App\Models\Plan;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PlanTest extends TestCase
{
    use DatabaseTransactions;
    /** @test */
    public function a_plan_can_add_some_gateways()
    {
        // Given
        $plan = factory(Plan::class)->create();
        $gateway = factory(Gateway::class)->make([
            'gateway_plan' => null
        ]);
        // When
        $plan->add($gateway);
        // Then
        $this->assertEquals(1,$plan->gateways->count());
    }
}